#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: LimeSDR FM transmitter
# Author: Hygn (Si-yeong Jang)
# GNU Radio version: 3.7.13.5
##################################################

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print "Warning: failed to XInitThreads()"

from PyQt4 import Qt
from PyQt4.QtCore import QObject, pyqtSlot
from gnuradio import analog
from gnuradio import audio
from gnuradio import blocks
from gnuradio import digital
from gnuradio import eng_notation
from gnuradio import filter
from gnuradio import gr
from gnuradio import qtgui
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from gnuradio.qtgui import Range, RangeWidget
from optparse import OptionParser
import limesdr
import rds
import sip
import sys
from gnuradio import qtgui


class LimeSDR_FM_transmitter(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "LimeSDR FM transmitter")
        Qt.QWidget.__init__(self)
        self.setWindowTitle("LimeSDR FM transmitter")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except:
            pass
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "LimeSDR_FM_transmitter")
        self.restoreGeometry(self.settings.value("geometry").toByteArray())


        ##################################################
        # Variables
        ##################################################
        self.stereo = stereo = 0
        self.sens = sens = 1
        self.samp_rate = samp_rate = 480e3
        self.rds_v = rds_v = 0
        self.premph = premph = 75e-6
        self.mod = mod = 0
        self.freq = freq = 103.3
        self.audiocut = audiocut = 15e3
        self.IQAttn = IQAttn = 1
        self.AudioAttn = AudioAttn = 0.8

        ##################################################
        # Blocks
        ##################################################
        self._stereo_options = (0, 1, )
        self._stereo_labels = ('Disable', 'Enable', )
        self._stereo_tool_bar = Qt.QToolBar(self)
        self._stereo_tool_bar.addWidget(Qt.QLabel("stereo"+": "))
        self._stereo_combo_box = Qt.QComboBox()
        self._stereo_tool_bar.addWidget(self._stereo_combo_box)
        for label in self._stereo_labels: self._stereo_combo_box.addItem(label)
        self._stereo_callback = lambda i: Qt.QMetaObject.invokeMethod(self._stereo_combo_box, "setCurrentIndex", Qt.Q_ARG("int", self._stereo_options.index(i)))
        self._stereo_callback(self.stereo)
        self._stereo_combo_box.currentIndexChanged.connect(
        	lambda i: self.set_stereo(self._stereo_options[i]))
        self.top_grid_layout.addWidget(self._stereo_tool_bar)
        self._sens_range = Range(0, 5, 0.1, 1, 200)
        self._sens_win = RangeWidget(self._sens_range, self.set_sens, 'FM sensitivity', "counter_slider", float)
        self.top_grid_layout.addWidget(self._sens_win)
        self._rds_v_options = (0, 1, )
        self._rds_v_labels = ('no', 'yes', )
        self._rds_v_tool_bar = Qt.QToolBar(self)
        self._rds_v_tool_bar.addWidget(Qt.QLabel('RDS'+": "))
        self._rds_v_combo_box = Qt.QComboBox()
        self._rds_v_tool_bar.addWidget(self._rds_v_combo_box)
        for label in self._rds_v_labels: self._rds_v_combo_box.addItem(label)
        self._rds_v_callback = lambda i: Qt.QMetaObject.invokeMethod(self._rds_v_combo_box, "setCurrentIndex", Qt.Q_ARG("int", self._rds_v_options.index(i)))
        self._rds_v_callback(self.rds_v)
        self._rds_v_combo_box.currentIndexChanged.connect(
        	lambda i: self.set_rds_v(self._rds_v_options[i]))
        self.top_grid_layout.addWidget(self._rds_v_tool_bar)
        self._premph_options = (75e-6, 50e-6, 0, )
        self._premph_labels = ('75us', '50us', '0', )
        self._premph_tool_bar = Qt.QToolBar(self)
        self._premph_tool_bar.addWidget(Qt.QLabel('Preemphasis'+": "))
        self._premph_combo_box = Qt.QComboBox()
        self._premph_tool_bar.addWidget(self._premph_combo_box)
        for label in self._premph_labels: self._premph_combo_box.addItem(label)
        self._premph_callback = lambda i: Qt.QMetaObject.invokeMethod(self._premph_combo_box, "setCurrentIndex", Qt.Q_ARG("int", self._premph_options.index(i)))
        self._premph_callback(self.premph)
        self._premph_combo_box.currentIndexChanged.connect(
        	lambda i: self.set_premph(self._premph_options[i]))
        self.top_grid_layout.addWidget(self._premph_tool_bar)
        self._mod_options = (0, 1, )
        self._mod_labels = ('FM', 'AM', )
        self._mod_tool_bar = Qt.QToolBar(self)
        self._mod_tool_bar.addWidget(Qt.QLabel('modulation'+": "))
        self._mod_combo_box = Qt.QComboBox()
        self._mod_tool_bar.addWidget(self._mod_combo_box)
        for label in self._mod_labels: self._mod_combo_box.addItem(label)
        self._mod_callback = lambda i: Qt.QMetaObject.invokeMethod(self._mod_combo_box, "setCurrentIndex", Qt.Q_ARG("int", self._mod_options.index(i)))
        self._mod_callback(self.mod)
        self._mod_combo_box.currentIndexChanged.connect(
        	lambda i: self.set_mod(self._mod_options[i]))
        self.top_grid_layout.addWidget(self._mod_tool_bar)
        self._freq_range = Range(20, 3000, 0.1, 103.3, 200)
        self._freq_win = RangeWidget(self._freq_range, self.set_freq, 'Center Frequency', "counter_slider", float)
        self.top_grid_layout.addWidget(self._freq_win)
        self._audiocut_range = Range(1e3, 24e3, 1, 15e3, 200)
        self._audiocut_win = RangeWidget(self._audiocut_range, self.set_audiocut, 'Audio cutoff', "counter_slider", float)
        self.top_grid_layout.addWidget(self._audiocut_win)
        self._IQAttn_range = Range(0, 1, 0.001, 1, 200)
        self._IQAttn_win = RangeWidget(self._IQAttn_range, self.set_IQAttn, 'IQ Atten', "counter_slider", float)
        self.top_grid_layout.addWidget(self._IQAttn_win)
        self._AudioAttn_range = Range(0, 4, 0.001, 0.8, 200)
        self._AudioAttn_win = RangeWidget(self._AudioAttn_range, self.set_AudioAttn, 'Audio Atten', "counter_slider", float)
        self.top_grid_layout.addWidget(self._AudioAttn_win)
        self.rational_resampler_xxx_2 = filter.rational_resampler_fff(
                interpolation=int(samp_rate / 48e3),
                decimation=1,
                taps=None,
                fractional_bw=None,
        )
        self.rational_resampler_xxx_1 = filter.rational_resampler_fff(
                interpolation=int(samp_rate / 48e3),
                decimation=1,
                taps=None,
                fractional_bw=None,
        )
        self.rational_resampler_xxx_0 = filter.rational_resampler_ccf(
                interpolation=25,
                decimation=24,
                taps=None,
                fractional_bw=None,
        )
        self.qtgui_time_sink_x_0 = qtgui.time_sink_f(
        	1024, #size
        	samp_rate, #samp_rate
        	"", #name
        	1 #number of inputs
        )
        self.qtgui_time_sink_x_0.set_update_time(0.10)
        self.qtgui_time_sink_x_0.set_y_axis(-1, 1)

        self.qtgui_time_sink_x_0.set_y_label('Amplitude', "")

        self.qtgui_time_sink_x_0.enable_tags(-1, True)
        self.qtgui_time_sink_x_0.set_trigger_mode(qtgui.TRIG_MODE_FREE, qtgui.TRIG_SLOPE_POS, 0.0, 0, 0, "")
        self.qtgui_time_sink_x_0.enable_autoscale(False)
        self.qtgui_time_sink_x_0.enable_grid(False)
        self.qtgui_time_sink_x_0.enable_axis_labels(True)
        self.qtgui_time_sink_x_0.enable_control_panel(False)
        self.qtgui_time_sink_x_0.enable_stem_plot(False)

        if not True:
          self.qtgui_time_sink_x_0.disable_legend()

        labels = ['', '', '', '', '',
                  '', '', '', '', '']
        widths = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        colors = ["blue", "red", "green", "black", "cyan",
                  "magenta", "yellow", "dark red", "dark green", "blue"]
        styles = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        markers = [-1, -1, -1, -1, -1,
                   -1, -1, -1, -1, -1]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]

        for i in xrange(1):
            if len(labels[i]) == 0:
                self.qtgui_time_sink_x_0.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_time_sink_x_0.set_line_label(i, labels[i])
            self.qtgui_time_sink_x_0.set_line_width(i, widths[i])
            self.qtgui_time_sink_x_0.set_line_color(i, colors[i])
            self.qtgui_time_sink_x_0.set_line_style(i, styles[i])
            self.qtgui_time_sink_x_0.set_line_marker(i, markers[i])
            self.qtgui_time_sink_x_0.set_line_alpha(i, alphas[i])

        self._qtgui_time_sink_x_0_win = sip.wrapinstance(self.qtgui_time_sink_x_0.pyqwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qtgui_time_sink_x_0_win)
        self.qtgui_freq_sink_x_0 = qtgui.freq_sink_f(
        	2048, #size
        	firdes.WIN_BLACKMAN_hARRIS, #wintype
        	0, #fc
        	samp_rate, #bw
        	"", #name
        	1 #number of inputs
        )
        self.qtgui_freq_sink_x_0.set_update_time(0.10)
        self.qtgui_freq_sink_x_0.set_y_axis(-140, 10)
        self.qtgui_freq_sink_x_0.set_y_label('Relative Gain', 'dB')
        self.qtgui_freq_sink_x_0.set_trigger_mode(qtgui.TRIG_MODE_FREE, 0.0, 0, "")
        self.qtgui_freq_sink_x_0.enable_autoscale(False)
        self.qtgui_freq_sink_x_0.enable_grid(False)
        self.qtgui_freq_sink_x_0.set_fft_average(1.0)
        self.qtgui_freq_sink_x_0.enable_axis_labels(True)
        self.qtgui_freq_sink_x_0.enable_control_panel(False)

        if not True:
          self.qtgui_freq_sink_x_0.disable_legend()

        if "float" == "float" or "float" == "msg_float":
          self.qtgui_freq_sink_x_0.set_plot_pos_half(not False)

        labels = ['', '', '', '', '',
                  '', '', '', '', '']
        widths = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        colors = ["blue", "red", "green", "black", "cyan",
                  "magenta", "yellow", "dark red", "dark green", "dark blue"]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]
        for i in xrange(1):
            if len(labels[i]) == 0:
                self.qtgui_freq_sink_x_0.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_freq_sink_x_0.set_line_label(i, labels[i])
            self.qtgui_freq_sink_x_0.set_line_width(i, widths[i])
            self.qtgui_freq_sink_x_0.set_line_color(i, colors[i])
            self.qtgui_freq_sink_x_0.set_line_alpha(i, alphas[i])

        self._qtgui_freq_sink_x_0_win = sip.wrapinstance(self.qtgui_freq_sink_x_0.pyqwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qtgui_freq_sink_x_0_win)
        self.low_pass_filter_0_0_0 = filter.fir_filter_fff(1, firdes.low_pass(
        	1, samp_rate, 2.5e3, 0.5e3, firdes.WIN_HAMMING, 6.76))
        self.low_pass_filter_0_0 = filter.fir_filter_fff(1, firdes.low_pass(
        	0.5, samp_rate, audiocut, 1e3, firdes.WIN_HAMMING, 6.76))
        self.low_pass_filter_0 = filter.fir_filter_fff(1, firdes.low_pass(
        	0.5, samp_rate, audiocut, 1e3, firdes.WIN_HAMMING, 6.76))
        self.limesdr_sink_0 = limesdr.sink('1D3AD61B92ECC6', 0, '', '')
        self.limesdr_sink_0.set_sample_rate(samp_rate/24*25)
        self.limesdr_sink_0.set_oversampling(8)
        self.limesdr_sink_0.set_center_freq(freq * 1e6, 0)
        self.limesdr_sink_0.set_bandwidth(5e6,0)
        self.limesdr_sink_0.set_gain(60,0)
        self.limesdr_sink_0.set_antenna(255,0)
        self.limesdr_sink_0.calibrate(4e6, 0)

        self.gr_rds_encoder_0 = rds.encoder(0, 0, True, 'WDR 3', freq*1e6,
        			True, False, 5, 0,
        			147, 'GNU Radio <3')

        self.digital_map_bb_1 = digital.map_bb((-1,1))
        self.digital_map_bb_0 = digital.map_bb((1,2))
        self.digital_diff_decoder_bb_0 = digital.diff_decoder_bb(2)
        self.blocks_unpack_k_bits_bb_0 = blocks.unpack_k_bits_bb(2)
        self.blocks_sub_xx_0 = blocks.sub_ff(1)
        self.blocks_repeat_0 = blocks.repeat(gr.sizeof_float*1, 160)
        self.blocks_multiply_xx_1 = blocks.multiply_vff(1)
        self.blocks_multiply_xx_0 = blocks.multiply_vff(1)
        self.blocks_multiply_const_vxx_3_0 = blocks.multiply_const_vff((mod, ))
        self.blocks_multiply_const_vxx_3 = blocks.multiply_const_vff((not(mod), ))
        self.blocks_multiply_const_vxx_2 = blocks.multiply_const_vff((0.15 * rds_v, ))
        self.blocks_multiply_const_vxx_1 = blocks.multiply_const_vcc((IQAttn, ))
        self.blocks_multiply_const_vxx_0_0 = blocks.multiply_const_vff((AudioAttn * stereo, ))
        self.blocks_multiply_const_vxx_0 = blocks.multiply_const_vff((AudioAttn, ))
        self.blocks_float_to_complex_1 = blocks.float_to_complex(1)
        self.blocks_char_to_float_0 = blocks.char_to_float(1, 1)
        self.blocks_add_xx_2 = blocks.add_vff(1)
        self.blocks_add_xx_1 = blocks.add_vcc(1)
        self.blocks_add_xx_0 = blocks.add_vff(1)
        self.audio_source_0 = audio.source(48000, '', True)
        self.analog_sig_source_x_2 = analog.sig_source_f(samp_rate, analog.GR_SIN_WAVE, 57e3, 1, 0)
        self.analog_sig_source_x_1 = analog.sig_source_f(samp_rate, analog.GR_SIN_WAVE, 19e3, stereo * 0.2, 0)
        self.analog_sig_source_x_0 = analog.sig_source_f(samp_rate, analog.GR_SIN_WAVE, 38e3, 0.5, 0)
        self.analog_frequency_modulator_fc_0 = analog.frequency_modulator_fc(sens)
        self.analog_fm_preemph_0_0 = analog.fm_preemph(fs=48e3, tau=premph, fh=-1.0)
        self.analog_fm_preemph_0 = analog.fm_preemph(fs=48e3, tau=premph, fh=-1.0)



        ##################################################
        # Connections
        ##################################################
        self.connect((self.analog_fm_preemph_0, 0), (self.rational_resampler_xxx_1, 0))
        self.connect((self.analog_fm_preemph_0_0, 0), (self.rational_resampler_xxx_2, 0))
        self.connect((self.analog_frequency_modulator_fc_0, 0), (self.blocks_add_xx_1, 1))
        self.connect((self.analog_sig_source_x_0, 0), (self.blocks_multiply_xx_0, 0))
        self.connect((self.analog_sig_source_x_1, 0), (self.blocks_add_xx_2, 2))
        self.connect((self.analog_sig_source_x_2, 0), (self.blocks_multiply_xx_1, 0))
        self.connect((self.audio_source_0, 1), (self.analog_fm_preemph_0, 0))
        self.connect((self.audio_source_0, 0), (self.analog_fm_preemph_0_0, 0))
        self.connect((self.blocks_add_xx_0, 0), (self.blocks_multiply_const_vxx_0, 0))
        self.connect((self.blocks_add_xx_1, 0), (self.rational_resampler_xxx_0, 0))
        self.connect((self.blocks_add_xx_2, 0), (self.blocks_multiply_const_vxx_3, 0))
        self.connect((self.blocks_add_xx_2, 0), (self.blocks_multiply_const_vxx_3_0, 0))
        self.connect((self.blocks_add_xx_2, 0), (self.qtgui_freq_sink_x_0, 0))
        self.connect((self.blocks_add_xx_2, 0), (self.qtgui_time_sink_x_0, 0))
        self.connect((self.blocks_char_to_float_0, 0), (self.blocks_repeat_0, 0))
        self.connect((self.blocks_float_to_complex_1, 0), (self.blocks_add_xx_1, 0))
        self.connect((self.blocks_multiply_const_vxx_0, 0), (self.blocks_add_xx_2, 1))
        self.connect((self.blocks_multiply_const_vxx_0_0, 0), (self.blocks_add_xx_2, 0))
        self.connect((self.blocks_multiply_const_vxx_1, 0), (self.limesdr_sink_0, 0))
        self.connect((self.blocks_multiply_const_vxx_2, 0), (self.blocks_add_xx_2, 3))
        self.connect((self.blocks_multiply_const_vxx_3, 0), (self.analog_frequency_modulator_fc_0, 0))
        self.connect((self.blocks_multiply_const_vxx_3_0, 0), (self.blocks_float_to_complex_1, 0))
        self.connect((self.blocks_multiply_xx_0, 0), (self.blocks_multiply_const_vxx_0_0, 0))
        self.connect((self.blocks_multiply_xx_1, 0), (self.blocks_multiply_const_vxx_2, 0))
        self.connect((self.blocks_repeat_0, 0), (self.low_pass_filter_0_0_0, 0))
        self.connect((self.blocks_sub_xx_0, 0), (self.blocks_multiply_xx_0, 1))
        self.connect((self.blocks_unpack_k_bits_bb_0, 0), (self.digital_map_bb_1, 0))
        self.connect((self.digital_diff_decoder_bb_0, 0), (self.digital_map_bb_0, 0))
        self.connect((self.digital_map_bb_0, 0), (self.blocks_unpack_k_bits_bb_0, 0))
        self.connect((self.digital_map_bb_1, 0), (self.blocks_char_to_float_0, 0))
        self.connect((self.gr_rds_encoder_0, 0), (self.digital_diff_decoder_bb_0, 0))
        self.connect((self.low_pass_filter_0, 0), (self.blocks_add_xx_0, 0))
        self.connect((self.low_pass_filter_0, 0), (self.blocks_sub_xx_0, 0))
        self.connect((self.low_pass_filter_0_0, 0), (self.blocks_add_xx_0, 1))
        self.connect((self.low_pass_filter_0_0, 0), (self.blocks_sub_xx_0, 1))
        self.connect((self.low_pass_filter_0_0_0, 0), (self.blocks_multiply_xx_1, 1))
        self.connect((self.rational_resampler_xxx_0, 0), (self.blocks_multiply_const_vxx_1, 0))
        self.connect((self.rational_resampler_xxx_1, 0), (self.low_pass_filter_0_0, 0))
        self.connect((self.rational_resampler_xxx_2, 0), (self.low_pass_filter_0, 0))

    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "LimeSDR_FM_transmitter")
        self.settings.setValue("geometry", self.saveGeometry())
        event.accept()

    def get_stereo(self):
        return self.stereo

    def set_stereo(self, stereo):
        self.stereo = stereo
        self._stereo_callback(self.stereo)
        self.blocks_multiply_const_vxx_0_0.set_k((self.AudioAttn * self.stereo, ))
        self.analog_sig_source_x_1.set_amplitude(self.stereo * 0.2)

    def get_sens(self):
        return self.sens

    def set_sens(self, sens):
        self.sens = sens
        self.analog_frequency_modulator_fc_0.set_sensitivity(self.sens)

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.qtgui_time_sink_x_0.set_samp_rate(self.samp_rate)
        self.qtgui_freq_sink_x_0.set_frequency_range(0, self.samp_rate)
        self.low_pass_filter_0_0_0.set_taps(firdes.low_pass(1, self.samp_rate, 2.5e3, 0.5e3, firdes.WIN_HAMMING, 6.76))
        self.low_pass_filter_0_0.set_taps(firdes.low_pass(0.5, self.samp_rate, self.audiocut, 1e3, firdes.WIN_HAMMING, 6.76))
        self.low_pass_filter_0.set_taps(firdes.low_pass(0.5, self.samp_rate, self.audiocut, 1e3, firdes.WIN_HAMMING, 6.76))
        self.analog_sig_source_x_2.set_sampling_freq(self.samp_rate)
        self.analog_sig_source_x_1.set_sampling_freq(self.samp_rate)
        self.analog_sig_source_x_0.set_sampling_freq(self.samp_rate)

    def get_rds_v(self):
        return self.rds_v

    def set_rds_v(self, rds_v):
        self.rds_v = rds_v
        self._rds_v_callback(self.rds_v)
        self.blocks_multiply_const_vxx_2.set_k((0.15 * self.rds_v, ))

    def get_premph(self):
        return self.premph

    def set_premph(self, premph):
        self.premph = premph
        self._premph_callback(self.premph)

    def get_mod(self):
        return self.mod

    def set_mod(self, mod):
        self.mod = mod
        self._mod_callback(self.mod)
        self.blocks_multiply_const_vxx_3_0.set_k((self.mod, ))
        self.blocks_multiply_const_vxx_3.set_k((not(self.mod), ))

    def get_freq(self):
        return self.freq

    def set_freq(self, freq):
        self.freq = freq
        self.limesdr_sink_0.set_center_freq(self.freq * 1e6, 0)

    def get_audiocut(self):
        return self.audiocut

    def set_audiocut(self, audiocut):
        self.audiocut = audiocut
        self.low_pass_filter_0_0.set_taps(firdes.low_pass(0.5, self.samp_rate, self.audiocut, 1e3, firdes.WIN_HAMMING, 6.76))
        self.low_pass_filter_0.set_taps(firdes.low_pass(0.5, self.samp_rate, self.audiocut, 1e3, firdes.WIN_HAMMING, 6.76))

    def get_IQAttn(self):
        return self.IQAttn

    def set_IQAttn(self, IQAttn):
        self.IQAttn = IQAttn
        self.blocks_multiply_const_vxx_1.set_k((self.IQAttn, ))

    def get_AudioAttn(self):
        return self.AudioAttn

    def set_AudioAttn(self, AudioAttn):
        self.AudioAttn = AudioAttn
        self.blocks_multiply_const_vxx_0_0.set_k((self.AudioAttn * self.stereo, ))
        self.blocks_multiply_const_vxx_0.set_k((self.AudioAttn, ))


def main(top_block_cls=LimeSDR_FM_transmitter, options=None):

    from distutils.version import StrictVersion
    if StrictVersion(Qt.qVersion()) >= StrictVersion("4.5.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()
    tb.start()
    tb.show()

    def quitting():
        tb.stop()
        tb.wait()
    qapp.connect(qapp, Qt.SIGNAL("aboutToQuit()"), quitting)
    qapp.exec_()


if __name__ == '__main__':
    main()
