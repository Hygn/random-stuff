#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Top Block
# GNU Radio version: 3.7.13.5
##################################################

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print "Warning: failed to XInitThreads()"

from PyQt4 import Qt
from PyQt4.QtCore import QObject, pyqtSlot
from gnuradio import audio
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import filter
from gnuradio import gr
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from gnuradio.qtgui import Range, RangeWidget
from optparse import OptionParser
import limesdr
import sys
from gnuradio import qtgui


class top_block(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "Top Block")
        Qt.QWidget.__init__(self)
        self.setWindowTitle("Top Block")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except:
            pass
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "top_block")
        self.restoreGeometry(self.settings.value("geometry").toByteArray())


        ##################################################
        # Variables
        ##################################################
        self.ssbdsb = ssbdsb = 1
        self.samp_rate = samp_rate = int(2e6)
        self.iqgain = iqgain = 1
        self.gain = gain = 1
        self.cut = cut = 6000
        self.carrier = carrier = 0.5

        ##################################################
        # Blocks
        ##################################################
        self._ssbdsb_options = (0, 1, )
        self._ssbdsb_labels = ('DSB', 'USB', )
        self._ssbdsb_tool_bar = Qt.QToolBar(self)
        self._ssbdsb_tool_bar.addWidget(Qt.QLabel('SSB orDSB'+": "))
        self._ssbdsb_combo_box = Qt.QComboBox()
        self._ssbdsb_tool_bar.addWidget(self._ssbdsb_combo_box)
        for label in self._ssbdsb_labels: self._ssbdsb_combo_box.addItem(label)
        self._ssbdsb_callback = lambda i: Qt.QMetaObject.invokeMethod(self._ssbdsb_combo_box, "setCurrentIndex", Qt.Q_ARG("int", self._ssbdsb_options.index(i)))
        self._ssbdsb_callback(self.ssbdsb)
        self._ssbdsb_combo_box.currentIndexChanged.connect(
        	lambda i: self.set_ssbdsb(self._ssbdsb_options[i]))
        self.top_grid_layout.addWidget(self._ssbdsb_tool_bar)
        self._iqgain_range = Range(0, 1, 0.001, 1, 200)
        self._iqgain_win = RangeWidget(self._iqgain_range, self.set_iqgain, 'IQ gain', "counter_slider", float)
        self.top_grid_layout.addWidget(self._iqgain_win)
        self._gain_range = Range(0, 3, 0.1, 1, 200)
        self._gain_win = RangeWidget(self._gain_range, self.set_gain, 'audio gain', "counter_slider", float)
        self.top_grid_layout.addWidget(self._gain_win)
        self._cut_range = Range(0, 12000, 1, 6000, 200)
        self._cut_win = RangeWidget(self._cut_range, self.set_cut, 'cutoff freq', "counter_slider", float)
        self.top_grid_layout.addWidget(self._cut_win)
        self._carrier_options = (0.5, 0, )
        self._carrier_labels = ('yes', 'no', )
        self._carrier_tool_bar = Qt.QToolBar(self)
        self._carrier_tool_bar.addWidget(Qt.QLabel('carrier '+": "))
        self._carrier_combo_box = Qt.QComboBox()
        self._carrier_tool_bar.addWidget(self._carrier_combo_box)
        for label in self._carrier_labels: self._carrier_combo_box.addItem(label)
        self._carrier_callback = lambda i: Qt.QMetaObject.invokeMethod(self._carrier_combo_box, "setCurrentIndex", Qt.Q_ARG("int", self._carrier_options.index(i)))
        self._carrier_callback(self.carrier)
        self._carrier_combo_box.currentIndexChanged.connect(
        	lambda i: self.set_carrier(self._carrier_options[i]))
        self.top_grid_layout.addWidget(self._carrier_tool_bar)
        self.rational_resampler_xxx_0 = filter.rational_resampler_fff(
                interpolation=samp_rate / 16 / 500,
                decimation=24,
                taps=None,
                fractional_bw=None,
        )
        self.low_pass_filter_0 = filter.fir_filter_fff(1, firdes.low_pass(
        	0.5, 24000, cut, 1000, firdes.WIN_HAMMING, 6.76))
        self.limesdr_sink_0 = limesdr.sink('1D3AD61B92ECC6', 0, '', '')
        self.limesdr_sink_0.set_sample_rate(samp_rate / 8)
        self.limesdr_sink_0.set_oversampling(16)
        self.limesdr_sink_0.set_center_freq(27.3e6, 0)
        self.limesdr_sink_0.set_bandwidth(5e6,0)
        self.limesdr_sink_0.set_gain(60,0)
        self.limesdr_sink_0.set_antenna(255,0)
        self.limesdr_sink_0.calibrate(4e6, 0)

        self.hilbert_fc_0 = filter.hilbert_fc(65, firdes.WIN_HAMMING, 6.76)
        self.blocks_multiply_const_xx_0 = blocks.multiply_const_ff(gain)
        self.blocks_multiply_const_vxx_1 = blocks.multiply_const_vcc((iqgain, ))
        self.blocks_multiply_const_vxx_0 = blocks.multiply_const_vff((ssbdsb, ))
        self.blocks_float_to_complex_0 = blocks.float_to_complex(1)
        self.blocks_complex_to_float_0 = blocks.complex_to_float(1)
        self.blocks_add_const_vxx_1 = blocks.add_const_vff((carrier, ))
        self.blocks_add_const_vxx_0 = blocks.add_const_vcc((carrier, ))
        self.audio_source_0 = audio.source(24000, '', True)



        ##################################################
        # Connections
        ##################################################
        self.connect((self.audio_source_0, 0), (self.low_pass_filter_0, 0))
        self.connect((self.blocks_add_const_vxx_0, 0), (self.blocks_complex_to_float_0, 0))
        self.connect((self.blocks_add_const_vxx_1, 0), (self.blocks_multiply_const_vxx_0, 0))
        self.connect((self.blocks_complex_to_float_0, 1), (self.blocks_add_const_vxx_1, 0))
        self.connect((self.blocks_complex_to_float_0, 0), (self.blocks_float_to_complex_0, 0))
        self.connect((self.blocks_float_to_complex_0, 0), (self.blocks_multiply_const_vxx_1, 0))
        self.connect((self.blocks_multiply_const_vxx_0, 0), (self.blocks_float_to_complex_0, 1))
        self.connect((self.blocks_multiply_const_vxx_1, 0), (self.limesdr_sink_0, 0))
        self.connect((self.blocks_multiply_const_xx_0, 0), (self.rational_resampler_xxx_0, 0))
        self.connect((self.hilbert_fc_0, 0), (self.blocks_add_const_vxx_0, 0))
        self.connect((self.low_pass_filter_0, 0), (self.blocks_multiply_const_xx_0, 0))
        self.connect((self.rational_resampler_xxx_0, 0), (self.hilbert_fc_0, 0))

    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "top_block")
        self.settings.setValue("geometry", self.saveGeometry())
        event.accept()

    def get_ssbdsb(self):
        return self.ssbdsb

    def set_ssbdsb(self, ssbdsb):
        self.ssbdsb = ssbdsb
        self._ssbdsb_callback(self.ssbdsb)
        self.blocks_multiply_const_vxx_0.set_k((self.ssbdsb, ))

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate

    def get_iqgain(self):
        return self.iqgain

    def set_iqgain(self, iqgain):
        self.iqgain = iqgain
        self.blocks_multiply_const_vxx_1.set_k((self.iqgain, ))

    def get_gain(self):
        return self.gain

    def set_gain(self, gain):
        self.gain = gain
        self.blocks_multiply_const_xx_0.set_k(self.gain)

    def get_cut(self):
        return self.cut

    def set_cut(self, cut):
        self.cut = cut
        self.low_pass_filter_0.set_taps(firdes.low_pass(0.5, 24000, self.cut, 1000, firdes.WIN_HAMMING, 6.76))

    def get_carrier(self):
        return self.carrier

    def set_carrier(self, carrier):
        self.carrier = carrier
        self._carrier_callback(self.carrier)
        self.blocks_add_const_vxx_1.set_k((self.carrier, ))
        self.blocks_add_const_vxx_0.set_k((self.carrier, ))


def main(top_block_cls=top_block, options=None):

    from distutils.version import StrictVersion
    if StrictVersion(Qt.qVersion()) >= StrictVersion("4.5.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()
    tb.start()
    tb.show()

    def quitting():
        tb.stop()
        tb.wait()
    qapp.connect(qapp, Qt.SIGNAL("aboutToQuit()"), quitting)
    qapp.exec_()


if __name__ == '__main__':
    main()
